package com.example.demo.controller;

import com.example.demo.entity.Product;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController
{
	@Autowired
	private MeterRegistry meterRegistry;

	@PostMapping(path= "/product", consumes = "application/json", produces = "application/json")
	public Product postProduct(@RequestBody Product product)
    {
		meterRegistry.counter("command_counter").increment();

		return product;
	}
}
