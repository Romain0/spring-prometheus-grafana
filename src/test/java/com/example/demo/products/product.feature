Feature: Product generator

    Background:
        * url 'http://localhost:8080'

    Scenario: Fetch random Product

        Given path 'product'
        When method POST
        Then status 200
        And match $ == {id:'#notnull'}