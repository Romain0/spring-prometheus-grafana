package com.example.demo.products;

import com.intuit.karate.junit5.Karate;

public class ProductsRunner
{
    @Karate.Test
    Karate testProduct() {
        return Karate.run("product").relativeTo(getClass());
    }
}
