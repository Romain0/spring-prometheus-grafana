package com.example.demo.unit.controllerTest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerTest
{
	@Autowired
	private MockMvc mvc;

	@Test
	void postProductTest() throws Exception
	{
		ResultActions req = mvc.perform(post("/product")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\n" +
						"    \"id\": 0,\n" +
						"    \"name\": \"abc\",\n" +
						"    \"client\": null\n" +
						"}"
				));

		req.andExpect(status().isOk());
	}
}
