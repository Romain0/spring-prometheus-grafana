# DevOps

## Mise en place des concepts DevOps
Ce TP à pour but de :
 - Vérifier la qualité de code
 - Vérification des dépendances (config. externe)
 - Vérification des conteneurs (config. externe)
 - Intégration continue (fichier / .jar)
 - Monitoring
 - Tests
   - Unitaires
   - Intégrations

### Gitlab-ci
Intégrer différentes pipelines dans un projet simple:

### Sonar
Vérifier la qualité du code

### Heroku
Découverte d'un hébergeur pratique pour le développement

### Prometheus & Grafana
Mise en place d'un système de monitoring avec métriques personnalisées

### Swagger
Ajout d'un Swagger au projet afin de visualiser l'API
